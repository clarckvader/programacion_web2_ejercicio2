﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebAppBooks.Models;

namespace WebAppBooks.Data
{
    public class WebAppBooksContext : DbContext
    {
        public WebAppBooksContext (DbContextOptions<WebAppBooksContext> options)
            : base(options)
        {
        }

        public DbSet<WebAppBooks.Models.Book> Book { get; set; }
    }
}
